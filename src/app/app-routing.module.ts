import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'admin-client',
    pathMatch: 'full'
  },
  {
    path: 'create-product',
    loadChildren: () => import('./pages/create-product/create-product.module').then( m => m.CreateProductPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'list-products-clients',
    loadChildren: () => import('./pages/list-products-clients/list-products-clients.module').then( m => m.ListProductsClientsPageModule)
  },
  {
    path: 'products-clients',
    loadChildren: () => import('./pages/products-clients/products-clients.module').then( m => m.ProductsClientsPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'edit-product/:id',
    loadChildren: () => import('./pages/edit-product/edit-product.module').then( m => m.EditProductPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'list-products',
    loadChildren: () => import('./pages/list-products/list-products.module').then( m => m.ListProductsPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'list-clients',
    loadChildren: () => import('./pages/list-clients/list-clients.module').then( m => m.ListClientsPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'map',
    loadChildren: () => import('./pages/map/map.module').then( m => m.MapPageModule)
  },
  {
    path: 'admin-client',
    loadChildren: () => import('./pages/admin-client/admin-client.module').then( m => m.AdminClientPageModule)
  },
  {
    path: 'create-client',
    loadChildren: () => import('./pages/create-client/create-client.module').then( m => m.CreateClientPageModule)
  },
  {
    path: 'detail-product',
    loadChildren: () => import('./pages/detail-product/detail-product.module').then( m => m.DetailProductPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
