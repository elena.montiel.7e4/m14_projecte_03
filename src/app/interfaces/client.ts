export interface Client {
    id: string;
    name: string;
    email: string;
    location: string;
 } 