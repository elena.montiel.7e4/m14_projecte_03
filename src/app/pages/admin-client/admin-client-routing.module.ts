import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminClientPage } from './admin-client.page';

const routes: Routes = [
  {
    path: '',
    component: AdminClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminClientPageRoutingModule {}
