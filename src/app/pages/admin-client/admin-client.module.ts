import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminClientPageRoutingModule } from './admin-client-routing.module';

import { AdminClientPage } from './admin-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminClientPageRoutingModule
  ],
  declarations: [AdminClientPage]
})
export class AdminClientPageModule {}
