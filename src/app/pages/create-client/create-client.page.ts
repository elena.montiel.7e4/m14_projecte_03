import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Product } from '../../interfaces/product';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.page.html',
  styleUrls: ['./create-client.page.scss'],
})
export class CreateClientPage implements OnInit {

  clientForm: FormGroup;
  products = [];

  constructor(private dataService: DataService, public formBuilder: FormBuilder, private router: Router) { }

  onSubmit() {
    if (!this.clientForm.valid) {
      return false;
    } else {
      this.dataService.createClient(this.clientForm.value)
      .then(() => {
        this.clientForm.reset();
        this.router.navigate(['/list-products-clients']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
 
  ngOnInit() {
    this.clientForm = this.formBuilder.group({
      name: [''],
      email: [''],
      location: ['']
    })
  }

}
