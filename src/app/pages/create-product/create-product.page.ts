import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.page.html',
  styleUrls: ['./create-product.page.scss'],
})
export class CreateProductPage implements OnInit {

  productForm: FormGroup;

  constructor(private dataService: DataService, public formBuilder: FormBuilder, private router: Router) { }

  onSubmit() {
    if (!this.productForm.valid) {
      return false;
    } else {
      this.dataService.createProduct(this.productForm.value)
      .then(() => {
        this.productForm.reset();
        this.router.navigate(['/list-products']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
 
  ngOnInit() {
    this.productForm = this.formBuilder.group({
      name: [''],
      description: [''],
      quantity: [''],
      price: [''],
      rating: ['']
    })
  }

}
