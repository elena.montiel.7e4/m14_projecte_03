import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.page.html',
  styleUrls: ['./edit-product.page.scss'],
})
export class EditProductPage implements OnInit {

  editForm: FormGroup;
  id: String;
  
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
        private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getProduct(this.id).subscribe(
      res => {
      this.editForm = this.formBuilder.group({
        name: [res['name']],
        description: [res['description']],
        quantity: [res['quantity']],
        price: [res['price']],
        rating: [res['rating']]
      })
    });
  }
 
  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: [''],
      description: [''],
      quantity: [''],
      price: [''],
      rating: ['']
    })
  }
 
  onSubmit() {
    this.dataService.updateProduct(this.id, this.editForm.value);
  }

}
