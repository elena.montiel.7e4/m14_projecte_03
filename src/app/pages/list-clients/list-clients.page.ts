import { Component, OnInit } from '@angular/core';

import { Client } from '../../interfaces/client';
import { DataService } from 'src/app/services/data.service';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.page.html',
  styleUrls: ['./list-clients.page.scss'],
})
export class ListClientsPage implements OnInit {

  clients = [];
 
  constructor(private dataService: DataService, public alertController: AlertController) { }

  async deleteClient(id){

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar cliente',
      message: '¿Está seguro de que quiere eliminar este cliente?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          id: 'cancel-button',
        }, {
          text: 'Sí',
          cssClass: 'success',
          id: 'confirm-button',
          handler: () => {
            this.dataService.deleteClient(id)
          }
        }
      ]
    });

    await alert.present();

  }
 
  ngOnInit() {
   this.dataService.getClients().subscribe(
     res => {
       this.clients = res.map((item) => {
         return {
           id: item.payload.doc.id,
           ... item.payload.doc.data() as Client
         };
       })
     }
   );
  }
}
