import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListProductsClientsPage } from './list-products-clients.page';

const routes: Routes = [
  {
    path: '',
    component: ListProductsClientsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListProductsClientsPageRoutingModule {}
