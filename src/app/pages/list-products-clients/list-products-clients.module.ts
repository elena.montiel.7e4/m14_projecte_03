import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListProductsClientsPageRoutingModule } from './list-products-clients-routing.module';

import { ListProductsClientsPage } from './list-products-clients.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListProductsClientsPageRoutingModule
  ],
  declarations: [ListProductsClientsPage]
})
export class ListProductsClientsPageModule {}
