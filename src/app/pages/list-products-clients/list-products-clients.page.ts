import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Product } from 'src/app/interfaces/product';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list-products-clients',
  templateUrl: './list-products-clients.page.html',
  styleUrls: ['./list-products-clients.page.scss'],
})
export class ListProductsClientsPage implements OnInit {

  products = [];
 
  constructor(private dataService: DataService, public alertController: AlertController, private router: Router) { }

  detailProduct(product){
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: product,
      }
    };
    
    this.router.navigate(['/detail-product'], navigationExtras);
  }

 
  ngOnInit() {
   this.dataService.getProducts().subscribe(
     res => {
       this.products = res.map((item) => {
         return {
           id: item.payload.doc.id,
           ... item.payload.doc.data() as Product
         };
       })
     }
   );
  }

}
