import { Component, OnInit } from '@angular/core';

import { Product } from '../../interfaces/product';
import { DataService } from 'src/app/services/data.service';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.page.html',
  styleUrls: ['./list-products.page.scss'],
})
export class ListProductsPage implements OnInit {

  products = [];
 
  constructor(private dataService: DataService, public alertController: AlertController) { }

  async deleteProduct(id){

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar producto',
      message: '¿Está seguro de que quiere eliminar este producto?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          id: 'cancel-button',
        }, {
          text: 'Sí',
          cssClass: 'success',
          id: 'confirm-button',
          handler: () => {
            this.dataService.deleteProduct(id)
          }
        }
      ]
    });

    await alert.present();

  }
 
  ngOnInit() {
   this.dataService.getProducts().subscribe(
     res => {
       this.products = res.map((item) => {
         return {
           id: item.payload.doc.id,
           ... item.payload.doc.data() as Product
         };
       })
     }
   );
  }

}
