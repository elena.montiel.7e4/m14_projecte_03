import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/authentication.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(public authService: AuthenticationService,
    public router: Router) { }

  ngOnInit() { }

  logIn(email, password) {
    this.authService.signIn(email.value, password.value)
      .then((res) => {
        this.router.navigateByUrl('/products-clients');
      }).catch((error) => {
        window.alert(error.message)
      })
  }
}