import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { Client } from '../../interfaces/client';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  location : any;
  longitude: any;
  latitude: any;

  mapboxgl: any;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  }

  ngOnInit() {
    console.log(this.location.adress);

    this.http.get<Client>('https://api.mapbox.com/geocoding/v5/mapbox.places/'+this.location.adress+".json?access_token=pk.eyJ1IjoiZWxlbmFtb250aWVsIiwiYSI6ImNsMjd0NGFsdjAyc3QzZXAxYTU0djQwMWYifQ.TC5V0Tbc-xPVWnTmeBSP9w").subscribe(resultados => {
     this.location = resultados;
     console.log(this.location); 
     this.longitude = this.location.features[0].center[0];
     this.latitude = this.location.features[0].center[1];
     //console.log(this.latitude)
     //console.log(this.longitude)
     
    this.mapboxgl.accessToken = environment.mapBoxToken;
    var map = new this.mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [2.1854994, 41.4532659],
      zoom: 14
    });

    map.on('load', () => {
      map.resize();
    });
    
    
    map.addControl(new this.mapboxgl.NavigationControl());

    var marker = new this.mapboxgl.Marker({})
     .setLngLat([this.location.features[0].center[1], this.location.features[0].center[0]])
     .setPopup(new this.mapboxgl.Popup().setHTML("<h1>Localización</h1>"))
     .addTo(map);
  })
  }

}
