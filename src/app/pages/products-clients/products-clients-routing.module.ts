import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsClientsPage } from './products-clients.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsClientsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsClientsPageRoutingModule {}
