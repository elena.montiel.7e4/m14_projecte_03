import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsClientsPageRoutingModule } from './products-clients-routing.module';

import { ProductsClientsPage } from './products-clients.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsClientsPageRoutingModule
  ],
  declarations: [ProductsClientsPage]
})
export class ProductsClientsPageModule {}
