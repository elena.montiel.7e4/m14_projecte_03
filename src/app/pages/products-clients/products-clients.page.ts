import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { AuthenticationService } from 'src/app/services/authentication.service';


@Component({
  selector: 'app-products-clients',
  templateUrl: './products-clients.page.html',
  styleUrls: ['./products-clients.page.scss'],
})
export class ProductsClientsPage implements OnInit {
 
  constructor(public ngFireAuth: AngularFireAuth, public router: Router) { }

  ngOnInit() {
  }

  signOut() {
    return this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  } 
}
