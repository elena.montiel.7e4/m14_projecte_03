import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Product } from '../interfaces/product';
import { Client } from '../interfaces/client';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  createProduct(product: Product) {
    return this.firestore.collection('products').add(product);
  }

  createClient(client: Product) {
    return this.firestore.collection('clients').add(client);
  }

  getProducts() {
    return this.firestore.collection('products').snapshotChanges();
  }
  
  getClients() {
    return this.firestore.collection('clients').snapshotChanges();
  }

  getProduct(id) {
    return this.firestore.collection('products').doc(id).valueChanges();
  }

  getClient(id) {
    return this.firestore.collection('clients').doc(id).valueChanges();
  }

  updateProduct(id, product: Product) {
    this.firestore.collection('products').doc(id).update(product)
      .then(() => {
        this.router.navigate(['list-products']);
      }).catch(error => console.log(error));
  }

  updateClient(id, client: Client) {
    this.firestore.collection('clients').doc(id).update(client)
      .then(() => {
        this.router.navigate(['list-clients']);
      }).catch(error => console.log(error));
  }

  deleteProduct(id) {
    this.firestore.doc('products/' + id).delete();
  }

  deleteClient(id) {
    this.firestore.doc('clients/' + id).delete();
  }

}
