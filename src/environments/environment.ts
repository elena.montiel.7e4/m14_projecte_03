// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBhxowwyHjTeJg46hApF7tpisErHUopcvY",
    authDomain: "fir-a53bd.firebaseapp.com",
    projectId: "fir-a53bd",
    storageBucket: "fir-a53bd.appspot.com",
    messagingSenderId: "67158982798",
    appId: "1:67158982798:web:72b2c2fcd95855077049df",
    measurementId: "G-ZH212T42FT"
  },
  mapBoxToken: "pk.eyJ1IjoiZWxlbmFtb250aWVsIiwiYSI6ImNsMjd0NGFsdjAyc3QzZXAxYTU0djQwMWYifQ.TC5V0Tbc-xPVWnTmeBSP9w"
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
